﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AT_Unit;

namespace AT_Unit.Tests
{
    class MockRepo : IRepo
    {
        public List<int> Values { get; set; }

        public List<int> NextInts()
        {
            return Values;
        }
    }
}
