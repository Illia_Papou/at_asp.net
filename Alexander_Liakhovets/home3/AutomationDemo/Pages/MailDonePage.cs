﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AutomationDemo.Pages
{
	public class MailDonePage : BaseMailPage
	{
		private static readonly By mailDoneMessageByCss = By.CssSelector("div.mail-Done-Title");
		private static readonly By mailNotificationByCss = By.CssSelector("div.mail-Notification-Close");

		public IWebElement MailDoneMessage { get { return browser.FindElement(mailDoneMessageByCss); } }
		public IWebElement MailNotification { get { return browser.FindElement(mailNotificationByCss); } }

		public bool IsDoneMessage()
		{
			return MailDoneMessage.Displayed;
		}

	}
}
