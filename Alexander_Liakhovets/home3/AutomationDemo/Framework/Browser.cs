﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace AutomationDemo.Framework
{
	public class Browser
	{
		private const string chromeDriverPath = "C:/";
		private static readonly TimeSpan implicitlyWait = TimeSpan.FromSeconds(10);
		private static readonly TimeSpan pageLoadWait = TimeSpan.FromSeconds(10);

		private IWebDriver driver;

		private static Lazy<Browser> instanceHolder = new Lazy<Browser>(() => new Browser());

		public static Browser Instance
		{
			get { return instanceHolder.Value; }
		}

		private Browser() { }

		public Browser Start()
		{
			// It isn't good practice to init and setup driver here this way.
			// Better use driver factories, config files etc. But for our purposes we can leave it for now.
			driver = new ChromeDriver(chromeDriverPath);
			driver.Manage().Timeouts().ImplicitlyWait(implicitlyWait);
			driver.Manage().Timeouts().SetPageLoadTimeout(pageLoadWait);
			driver.Manage().Window.Maximize();
			return this;
		}

		public void Close()
		{
			if (driver != null) driver.Close();
			driver = null;
		}

		public void Refresh() => driver.Navigate().Refresh();

		public void OpenAt(string url) => driver.Url = url;

		internal IWebElement FindElement(By by) => driver.FindElement(by);
	}
}
