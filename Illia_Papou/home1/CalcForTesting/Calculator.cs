﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalcForTesting
{
    public class Calculator
    {
        public int Sum(int a, int b)
        {
            return checked (a + b);
        }

        public int Subtraction(int a, int b)
        {
            return checked (a - b);
        }

        public int Multiply(int a, int b)
        {
            return checked (a * b);
        }

        public double Division(double a, double b)
        {
            if(b==0)
                throw new DivideByZeroException("Error. Division by null");

            return (a / b);
        }
    }
}
