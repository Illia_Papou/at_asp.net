﻿using NUnit.Framework;
using CalcForTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcForTesting.Test
{
    [TestFixture]
    public class CalcTests
    {

        private Calculator calc;

        [SetUp]
        public void SetUp()
        {
            calc = new Calculator();
        }


        [TestCase(2, 3, 5)]
        [TestCase(3, 3, 6)]
        [TestCase(0, 3, 3)]
        public void SumTest(int a, int b, int expected)
        {
            var actual = calc.Sum(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(2, 3, -1)]
        [TestCase(3, 3, 0)]
        [TestCase(0, 3, -3)]
        public void SubtractTest(int a, int b, int expected)
        {
            var actual = calc.Subtraction(a, b);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DivideTest_5divideTo0_throwsArgumentException()
        {
            double a = 5;
            double b = 0;

            Assert.Throws<DivideByZeroException>(() => calc.Division(a, b));
        }

        [TestCase(3, 3, 1)]
        [TestCase(9, 3, 3)]
        [TestCase(0, 3, 0)]
        public void DivideTest(double a, double b, double expected)
        {
            var actual = calc.Division(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(2, 3, 6)]
        [TestCase(3, 3, 9)]
        [TestCase(0, 3, 0)]
        public void MultipleTest(int a, int b, int expected)
        {
            var actual = calc.Multiply(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TearDown]
        public void TearDown()
        {
            calc = null;
        }

    }
}