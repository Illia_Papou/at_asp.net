﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkForAutomation.Models
{
    public class Letter
    {
        public string Email { get; set; }
        public string Title { get; set; }
        public string InputForText { get; set; }

        public Letter(string email, string title, string inputForText)
        {
            Email = email;
            Title = title;
            InputForText = inputForText;
        }
    }
}
