﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkForAutomation.Models
{
    public class UserAccount
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public UserAccount(string name, string password, string email)
        {
            Login = name;
            Password = password;
            Email = email;
        }
    }
}
